// Base Setup
const express = require('express');
const bodyParser = require('body-parser');

// Connect to Mongo local database (it could be a remote one from some Internet service)
const mongoose = require('mongoose');
mongoose.connect('mongodb://mongo-server-on-mean-system/mean-system?authSource=admin', { user: 'mongo', pass: 'secret' });

// Mongoose models
const Bear = require('./app/models/bear');


// Application settings
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Router object
const router = express.Router();
// Middleware to use for all requests
router.use((request, response, next) => {
    console.log('Something is happening!');
    next();	// it forwards requests, so it is VERY important!
});


// Routes
router.get('/', (request, response) => {
    response.json({ message: 'Hooray! Welcome to our API!' });
});

// Bears API (/api/bears)
router.route('/bears')
    // create a bear: POST http://localhost:8080/api/bears
    .post((request, response) => {
        let bear = new Bear();
		bear.name = request.body.name; // comes from the request

		bear.save((error) => {
			if (error) response.send(error);
			response.json({ message: `Bear ${bear.name} created!` });
		});
    })
    // get all the bears: GET http://localhost:8080/api/bears
    .get((request, response) => {
		Bear.find((error, bears) => {
		if (error) response.send(error);
			response.json(bears);
		});	
    });
	
// Bears API (/api/bears/:bear_api)
router.route('/bears/:bear_id')
    // get the bear with that id: GET http://localhost:8080/api/bears/:bear_id
    .get((request, response) => {
        Bear.findById(request.params.bear_id, (error, bear) => {
	    if (error) response.send(error);
			response.json(bear);
		});
    })
    // update the bear with this id: PUT http://localhost:8080/api/bears/:bear_id
    .put((request, response) => {
        // use our bear model to find the bear we want
		Bear.findById(request.params.bear_id, (error, bear) => {
			if (error) response.send(error);

			bear.name = request.body.name;

			// save the bear
			bear.save((error) => {
				if (error) response.send(error);
				response.json(`Bear ${bear.name} updated!`);
			});
		});
    })
    // delete the bear with this id
    .delete((request, response) => {
        Bear.remove({
			_id: request.params.bear_id
		}, (error, bear) => {
			if (error) response.send(error);
			response.json(`Bear successfully deleted!`);
		});
    });
	
// prefix all routes with /apimes
app.use('/api', router);

// Start server
// Constants
const PORT = 8080;
const HOST = '0.0.0.0';
app.listen(PORT, HOST);
console.log(`Magic happens at http://${HOST}:${PORT}`);
